resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/BMW330i/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/BMW330i/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/BMW330i/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/BMW330i/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/BMW330i/carvariations.meta'

files {
'data/BMW330i/handling.meta',
'data/BMW330i/vehicles.meta',
'data/BMW330i/carcols.meta',
'data/BMW330i/carvariations.meta',
'data/BMW330i/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/BMW330iE90/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/BMW330iE90/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/BMW330iE90/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/BMW330iE90/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/BMW330iE90/carvariations.meta'

files {
'data/BMW330iE90/handling.meta',
'data/BMW330iE90/vehicles.meta',
'data/BMW330iE90/carcols.meta',
'data/BMW330iE90/carvariations.meta',
'data/BMW330iE90/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/BMWM4GTS/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/BMWM4GTS/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/BMWM4GTS/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/BMWM4GTS/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/BMWM4GTS/carvariations.meta'

files {
'data/BMWM4GTS/handling.meta',
'data/BMWM4GTS/vehicles.meta',
'data/BMWM4GTS/carcols.meta',
'data/BMWM4GTS/carvariations.meta',
'data/BMWM4GTS/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/BugattiChiron/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/BugattiChiron/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/BugattiChiron/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/BugattiChiron/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/BugattiChiron/carvariations.meta'

files {
'data/BugattiChiron/handling.meta',
'data/BugattiChiron/vehicles.meta',
'data/BugattiChiron/carcols.meta',
'data/BugattiChiron/carvariations.meta',
'data/BugattiChiron/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/ChevroletCorvette/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/ChevroletCorvette/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/ChevroletCorvette/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/ChevroletCorvette/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/ChevroletCorvette/carvariations.meta'

files {
'data/ChevroletCorvette/handling.meta',
'data/ChevroletCorvette/vehicles.meta',
'data/ChevroletCorvette/carcols.meta',
'data/ChevroletCorvette/carvariations.meta',
'data/ChevroletCorvette/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Ferrari430/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Ferrari430/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Ferrari430/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Ferrari430/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Ferrari430/carvariations.meta'

files {
'data/Ferrari430/handling.meta',
'data/Ferrari430/vehicles.meta',
'data/Ferrari430/carcols.meta',
'data/Ferrari430/carvariations.meta',
'data/Ferrari430/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Ferrari488/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Ferrari488/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Ferrari488/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Ferrari488/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Ferrari488/carvariations.meta'

files {
'data/Ferrari488/handling.meta',
'data/Ferrari488/vehicles.meta',
'data/Ferrari488/carcols.meta',
'data/Ferrari488/carvariations.meta',
'data/Ferrari488/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Ferrari812/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Ferrari812/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Ferrari812/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Ferrari812/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Ferrari812/carvariations.meta'

files {
'data/Ferrari812/handling.meta',
'data/Ferrari812/vehicles.meta',
'data/Ferrari812/carcols.meta',
'data/Ferrari812/carvariations.meta',
'data/Ferrari812/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/FerrariGTC4lusso/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/FerrariGTC4lusso/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/FerrariGTC4lusso/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/FerrariGTC4lusso/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/FerrariGTC4lusso/carvariations.meta'

files {
'data/FerrariGTC4lusso/handling.meta',
'data/FerrariGTC4lusso/vehicles.meta',
'data/FerrariGTC4lusso/carcols.meta',
'data/FerrariGTC4lusso/carvariations.meta',
'data/FerrariGTC4lusso/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/FH/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/FH/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/FH/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/FH/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/FH/carvariations.meta'

files {
'data/FH/handling.meta',
'data/FH/vehicles.meta',
'data/FH/carcols.meta',
'data/FH/carvariations.meta',
'data/FH/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/FordGT2017/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/FordGT2017/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/FordGT2017/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/FordGT2017/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/FordGT2017/carvariations.meta'

files {
'data/FordGT2017/handling.meta',
'data/FordGT2017/vehicles.meta',
'data/FordGT2017/carcols.meta',
'data/FordGT2017/carvariations.meta',
'data/FordGT2017/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/GumpertApollo/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/GumpertApollo/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/GumpertApollo/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/GumpertApollo/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/GumpertApollo/carvariations.meta'

files {
'data/GumpertApollo/handling.meta',
'data/GumpertApollo/vehicles.meta',
'data/GumpertApollo/carcols.meta',
'data/GumpertApollo/carvariations.meta',
'data/GumpertApollo/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/JaguarProject7/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/JaguarProject7/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/JaguarProject7/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/JaguarProject7/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/JaguarProject7/carvariations.meta'

files {
'data/JaguarProject7/handling.meta',
'data/JaguarProject7/vehicles.meta',
'data/JaguarProject7/carcols.meta',
'data/JaguarProject7/carvariations.meta',
'data/JaguarProject7/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/JaguarSpace/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/JaguarSpace/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/JaguarSpace/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/JaguarSpace/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/JaguarSpace/carvariations.meta'

files {
'data/JaguarSpace/handling.meta',
'data/JaguarSpace/vehicles.meta',
'data/JaguarSpace/carcols.meta',
'data/JaguarSpace/carvariations.meta',
'data/JaguarSpace/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/JaguarXJ2010/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/JaguarXJ2010/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/JaguarXJ2010/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/JaguarXJ2010/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/JaguarXJ2010/carvariations.meta'

files {
'data/JaguarXJ2010/handling.meta',
'data/JaguarXJ2010/vehicles.meta',
'data/JaguarXJ2010/carcols.meta',
'data/JaguarXJ2010/carvariations.meta',
'data/JaguarXJ2010/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/JeepWrangler/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/JeepWrangler/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/JeepWrangler/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/JeepWrangler/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/JeepWrangler/carvariations.meta'

files {
'data/JeepWrangler/handling.meta',
'data/JeepWrangler/vehicles.meta',
'data/JeepWrangler/carcols.meta',
'data/JeepWrangler/carvariations.meta',
'data/JeepWrangler/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/LamborghiniHurucan/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/LamborghiniHurucan/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/LamborghiniHurucan/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/LamborghiniHurucan/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/LamborghiniHurucan/carvariations.meta'

files {
'data/LamborghiniHurucan/handling.meta',
'data/LamborghiniHurucan/vehicles.meta',
'data/LamborghiniHurucan/carcols.meta',
'data/LamborghiniHurucan/carvariations.meta',
'data/LamborghiniHurucan/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/LexusRCF/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/LexusRCF/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/LexusRCF/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/LexusRCF/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/LexusRCF/carvariations.meta'

files {
'data/LexusRCF/handling.meta',
'data/LexusRCF/vehicles.meta',
'data/LexusRCF/carcols.meta',
'data/LexusRCF/carvariations.meta',
'data/LexusRCF/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/McLaren720/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/McLaren720/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/McLaren720/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/McLaren720/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/McLaren720/carvariations.meta'

files {
'data/McLaren720/handling.meta',
'data/McLaren720/vehicles.meta',
'data/McLaren720/carcols.meta',
'data/McLaren720/carvariations.meta',
'data/McLaren720/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/McLarenMp4/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/McLarenMp4/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/McLarenMp4/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/McLarenMp4/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/McLarenMp4/carvariations.meta'

files {
'data/McLarenMp4/handling.meta',
'data/McLarenMp4/vehicles.meta',
'data/McLarenMp4/carcols.meta',
'data/McLarenMp4/carvariations.meta',
'data/McLarenMp4/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/MercedezAMGE63s/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/MercedezAMGE63s/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/MercedezAMGE63s/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/MercedezAMGE63s/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/MercedezAMGE63s/carvariations.meta'

files {
'data/MercedezAMGE63s/handling.meta',
'data/MercedezAMGE63s/vehicles.meta',
'data/MercedezAMGE63s/carcols.meta',
'data/MercedezAMGE63s/carvariations.meta',
'data/MercedezAMGE63s/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/MercedezAMGGTS/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/MercedezAMGGTS/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/MercedezAMGGTS/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/MercedezAMGGTS/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/MercedezAMGGTS/carvariations.meta'

files {
'data/MercedezAMGGTS/handling.meta',
'data/MercedezAMGGTS/vehicles.meta',
'data/MercedezAMGGTS/carcols.meta',
'data/MercedezAMGGTS/carvariations.meta',
'data/MercedezAMGGTS/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/MercedezG65/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/MercedezG65/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/MercedezG65/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/MercedezG65/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/MercedezG65/carvariations.meta'

files {
'data/MercedezG65/handling.meta',
'data/MercedezG65/vehicles.meta',
'data/MercedezG65/carcols.meta',
'data/MercedezG65/carvariations.meta',
'data/MercedezG65/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/MercedezMLBrabus2009/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/MercedezMLBrabus2009/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/MercedezMLBrabus2009/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/MercedezMLBrabus2009/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/MercedezMLBrabus2009/carvariations.meta'

files {
'data/MercedezMLBrabus2009/handling.meta',
'data/MercedezMLBrabus2009/vehicles.meta',
'data/MercedezMLBrabus2009/carcols.meta',
'data/MercedezMLBrabus2009/carvariations.meta',
'data/MercedezMLBrabus2009/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/MercedezS500L/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/MercedezS500L/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/MercedezS500L/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/MercedezS500L/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/MercedezS500L/carvariations.meta'

files {
'data/MercedezS500L/handling.meta',
'data/MercedezS500L/vehicles.meta',
'data/MercedezS500L/carcols.meta',
'data/MercedezS500L/carvariations.meta',
'data/MercedezS500L/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/PaganiHuayra/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/PaganiHuayra/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/PaganiHuayra/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/PaganiHuayra/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/PaganiHuayra/carvariations.meta'

files {
'data/PaganiHuayra/handling.meta',
'data/PaganiHuayra/vehicles.meta',
'data/PaganiHuayra/carcols.meta',
'data/PaganiHuayra/carvariations.meta',
'data/PaganiHuayra/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/Porshe918Spider/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Porshe918Spider/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Porshe918Spider/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Porshe918Spider/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Porshe918Spider/carvariations.meta'

files {
'data/Porshe918Spider/handling.meta',
'data/Porshe918Spider/vehicles.meta',
'data/Porshe918Spider/carcols.meta',
'data/Porshe918Spider/carvariations.meta',
'data/Porshe918Spider/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/PorsheMacan/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/PorsheMacan/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/PorsheMacan/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/PorsheMacan/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/PorsheMacan/carvariations.meta'

files {
'data/PorsheMacan/handling.meta',
'data/PorsheMacan/vehicles.meta',
'data/PorsheMacan/carcols.meta',
'data/PorsheMacan/carvariations.meta',
'data/PorsheMacan/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/PorsheP911R2016/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/PorsheP911R2016/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/PorsheP911R2016/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/PorsheP911R2016/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/PorsheP911R2016/carvariations.meta'

files {
'data/PorsheP911R2016/handling.meta',
'data/PorsheP911R2016/vehicles.meta',
'data/PorsheP911R2016/carcols.meta',
'data/PorsheP911R2016/carvariations.meta',
'data/PorsheP911R2016/vehiclelayouts.meta',
}
---------------------------------------------------------------------
data_file 'HANDLING_FILE' 'data/PorshePanamera/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/PorshePanamera/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/PorshePanamera/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/PorshePanamera/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/PorshePanamera/carvariations.meta'

files {
'data/PorshePanamera/handling.meta',
'data/PorshePanamera/vehicles.meta',
'data/PorshePanamera/carcols.meta',
'data/PorshePanamera/carvariations.meta',
'data/PorshePanamera/vehiclelayouts.meta',
}
---------------------------------------------------------------------

data_file 'HANDLING_FILE' 'data/renaultsportRS/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/renaultsportRS/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/renaultsportRS/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/renaultsportRS/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/renaultsportRS/carvariations.meta'

files {
'data/renaultsportRS/handling.meta',
'data/renaultsportRS/vehicles.meta',
'data/renaultsportRS/carcols.meta',
'data/renaultsportRS/carvariations.meta',
'data/renaultsportRS/vehiclelayouts.meta',
}
---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Ferrari458/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Ferrari458/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Ferrari458/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Ferrari458/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Ferrari458/carvariations.meta'

files {
'data/Ferrari458/handling.meta',
'data/Ferrari458/vehicles.meta',
'data/Ferrari458/carcols.meta',
'data/Ferrari458/carvariations.meta',
'data/Ferrari458/vehiclelayouts.meta',
}
---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/Mgt/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/Mgt/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/Mgt/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/Mgt/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/Mgt/carvariations.meta'

files {
'data/Mgt/handling.meta',
'data/Mgt/vehicles.meta',
'data/Mgt/carcols.meta',
'data/Mgt/carvariations.meta',
'data/Mgt/vehiclelayouts.meta',
}
---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/bmwi8/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/bmwi8/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/bmwi8/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/bmwi8/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/bmwi8/carvariations.meta'

files {
'data/bmwi8/handling.meta',
'data/bmwi8/vehicles.meta',
'data/bmwi8/carcols.meta',
'data/bmwi8/carvariations.meta',
'data/bmwi8/vehiclelayouts.meta',
}
---------------------------------------------------------------------


data_file 'HANDLING_FILE' 'data/rollsroycephantom/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/rollsroycephantom/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/rollsroycephantom/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/rollsroycephantom/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/rollsroycephantom/carvariations.meta'

files {
'data/rollsroycephantom/handling.meta',
'data/rollsroycephantom/vehicles.meta',
'data/rollsroycephantom/carcols.meta',
'data/rollsroycephantom/carvariations.meta',
'data/rollsroycephantom/vehiclelayouts.meta',
}
---------------------------------------------------------------------


<<<<<<< HEAD
data_file 'HANDLING_FILE' 'data/CB1000RNaked/handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'data/CB1000RNaked/vehicles.meta'
data_file 'CARCOLS_FILE' 'data/CB1000RNaked/carcols.meta'
data_file 'VEHICLE_LAYOUTS_FILE' 'data/CB1000RNaked/vehiclelayouts.meta'
data_file 'VEHICLE_VARIATION_FILE' 'data/CB1000RNaked/carvariations.meta'

files {
'data/CB1000RNaked/handling.meta',
'data/CB1000RNaked/vehicles.meta',
'data/CB1000RNaked/carcols.meta',
'data/CB1000RNaked/carvariations.meta',
'data/CB1000RNaked/vehiclelayouts.meta',
}
---------------------------------------------------------------------


=======
>>>>>>> 548672d819ee343adf09d7aa920de06504183733
client_script 'vehicle_names.lua'